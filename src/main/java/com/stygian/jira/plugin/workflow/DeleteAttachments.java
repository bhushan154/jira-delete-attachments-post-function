package com.stygian.jira.plugin.workflow;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import java.util.Collection;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class DeleteAttachments extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(DeleteAttachments.class);

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        AttachmentManager attachmentManager = ComponentAccessor.getAttachmentManager();
        Collection<Attachment> attachments = attachmentManager.getAttachments(issue);
        for(Attachment attachment:attachments){
            try
            {
                attachmentManager.deleteAttachment(attachment);
            }
            catch(RemoveException exc){
                log.error("Unable to delete attachment " + attachment.toString() + " from issue " + issue.getKey());
            }
        }
    }
}