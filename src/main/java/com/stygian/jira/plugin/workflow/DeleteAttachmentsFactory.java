package com.stygian.jira.plugin.workflow;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.*;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class DeleteAttachmentsFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory{

    private WorkflowManager workflowManager;


    public DeleteAttachmentsFactory(WorkflowManager workflowManager){
            this.workflowManager=workflowManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object>velocityParams){

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object>velocityParams,AbstractDescriptor descriptor){

        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object>velocityParams,AbstractDescriptor descriptor){
    }


    public Map<String,?>getDescriptorParams(Map<String, Object>formParams){
        Map params=new HashMap();
        // Process The map
        return params;
    }

}